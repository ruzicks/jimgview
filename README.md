Jimgview
========
Simple image viewer written in java. See example here:

![Example](doc/example.mp4)

Usage
-----
When the program is launched without arguments it opens the current folder,
otherwise it opens the directory or file specified in the first argument. You
can type path into the url bar at any time and load specified path by hitting
enter. Otherwise usage should be fairly straight forward, clicking on image or
directory in folder view opens it, you can use mouse wheel to zoom in/out, and
in the folder view you can use CTRL+scroll wheel to change image preview size.

Scaling method can be changed in the _View_ menu.

Shortcuts
---------

| Shortcut   | Action                                 |
|------------|----------------------------------------|
| home       | go up in the directory tree/from image view to folder view |
| w/s/a/d    | scrolling up/down/left/right           |
| up/down    | scrolling up/down                      |
| left/right | next/previous file                     |
| 0, =       | fit image in window                    |
| 1          | Set image zoom to 100%                 |
| enter      | open selected file (in folder view; files are selected with next/previous shortcut) |
| +/-        | zoom in/out, change image preview size (folder view) |
