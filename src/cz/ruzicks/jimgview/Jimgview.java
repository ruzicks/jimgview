/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.ruzicks.jimgview;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Container;
import java.awt.Insets;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.filechooser.FileNameExtensionFilter;

import cz.ruzicks.jimgview.component.FolderView;
import cz.ruzicks.jimgview.component.ImagePanel;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;
import javax.swing.*;

/**
 * The main window class. It takes care of control components, creating and
 * managing lifetime of the program and the components which themselves display
 * whatever is needed, and ties them together by managing and passing events and
 * actions.
 * 
 * @author Šimon Růžička
 */
public class Jimgview {
    public enum Action {
        SCROLL_LEFT, SCROLL_RIGHT, SCROLL_DOWN, SCROLL_UP,
        GO_PREV, GO_NEXT, GO_UP, ENTER,
        ZOOM_IN, ZOOM_OUT,
        ZOOM_1TO1, ZOOM_FIT
    };

    /* card layout constants */
    private static final String IMAGE_PANE = "image_pane";
    private static final String FOLDER_PANE = "folder_pane";

    /** the main window frame */
    private static JFrame frame;

    private static JMenuBar menuBar;
    private static JToolBar toolBar;
    /* toolbar components from the currently displayed content of main window */
    private static JComponent tmpToolBarComponents[] = null;
    private static JTextField urlBar;

    /**
     * The panel managing the inner pane(s) (folder and image view). It uses
     * card layout and always keeps the current {@link #folderView} and
     * {@link #imagePanel}, switching between them.
     */
    private static JPanel contentsPanel;
    private static CardLayout contentsLayout;
    private static ImagePanel imagePanel = null;
    /**
     * Swing component representing the folder; is created/removed together with
     * {@link #folder}
     */
    private static FolderView folderView = null;
    /* logical representation of the current folder */
    public static Folder folder = null;

    /** currently displayed file */
    private static File currentFile = null;
    /** the current card displayed by {@link #contentsPanel}/{@link #contentsLayout} */
    private static String currentCard = null;

    /**
     * Start the program using the first argument as path to display (can be
     * directory or image). If no argument is present, the current directory is
     * displayed.
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        final String path = (args.length > 0) ?
            args[0] : new File(".").getAbsoluteFile().getParent();
        javax.swing.SwingUtilities.invokeLater(() -> startGUI(path));
    }

    /**
     * Initialize swing components and load the specified path.
     * @param path The path to display (folder or image).
     */
    private static void startGUI(String path) {
        path = new File(path).getAbsolutePath();
        frame = new JFrame("JImgView");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        /* Let the frame handle and forward key events. */
        frame.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                Jimgview.keyPressed(e);
            }
        });
        /* save settings before exiting */
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                Settings.savePreferences();
                System.exit(0);
            }
        });
        
        Settings.loadPreferences();

        Container pane = frame.getContentPane();

        /* MENU BAR */

        menuBar = createMenuBar();
        frame.setJMenuBar(menuBar);

        toolBar = new JToolBar();
        toolBar.setFloatable(false);
        toolBar.setFocusable(false);
        frame.add(toolBar, BorderLayout.NORTH);

        /* URL BAR */

        urlBar = createUrlBar(path);
        toolBar.add(urlBar);

        /* 
         * CONTETNS PANEL
         *
         * The main view is handled by card layout, which always contains the
         * folder view and the file view and switches between those two.
         */

        contentsLayout = new CardLayout();
        contentsPanel = new JPanel(contentsLayout);
        frame.add(contentsPanel, BorderLayout.CENTER);

        frame.setBounds(100, 100, Settings.DEFAULT_WINDOW_WIDTH, Settings.DEFAULT_WINDOW_HEIGHT);
        frame.setVisible(true);
        frame.requestFocus();

        urlLoad();
    }

    /**
     * Open file chooser dialog and load the resulting image.
     */
    static void open() {
        JFileChooser chooser = new JFileChooser();
        chooser.setDialogTitle("open");
        chooser.setDialogType(JFileChooser.OPEN_DIALOG);
        chooser.setFileFilter(new FileNameExtensionFilter("image", "gif", "jpg", "jpeg", "png"));
        int ret = chooser.showOpenDialog(null);
        if (ret == JFileChooser.APPROVE_OPTION) {
            File imgFile = chooser.getSelectedFile();
            urlBar.setText(imgFile.getAbsolutePath());
            urlLoad();
        }
    }

    /**
     * Load specified path, showing it in appropriate window ({@link ImagePanel}
     * or {@link FolderView}).
     * @param path Path to load. Invalid path will be ignored.
     */
    public static void loadPath(File path) {
        urlBar.setText(path.getAbsolutePath());
        urlLoad();
    }

    /**
     * Load next image. If last image is currently loaded, cycles to the
     * beginning. The images are ordered by {@link File#compareTo(java.io.File)}
     * Current image is stored/handled by {@link #folder}. 
     */
    public static void next() {
        if (folder == null) return;
        File f = folder.nextImage();
        if (f != null) loadPath(f);
    }

    /**
     * Load previous image. If first image is currently loaded, cycles to the
     * end. The images are ordered by {@link File#compareTo(java.io.File)}
     * Current image is stored/handled by {@link #folder}. 
     */
    public static void prev() {
        if (folder == null) return;
        File f = folder.prevImage();
        if (f != null) loadPath(f);
    }

    /**
     * Load current url and use appropriate view depending on whether it is a
     * file or a directory. Loads file/directory specified in  {@link #urlBar}.
     * This is used by other functions which set contents of url bar and then
     * call this function. If the url is not valid, functions (re)sets url bar
     * text to the currently loaded file.
     */
    private static void urlLoad() {
        File urlFile = new File(urlBar.getText()).getAbsoluteFile();
        if (urlFile.isDirectory()) {
            loadFolder(urlFile);
            contentsLayout.show(contentsPanel, FOLDER_PANE);
            currentCard = FOLDER_PANE;
            setTmpToolBarComponents(folderView.getToolBarItems());
            folderView.startIconWorker();
        } else if (urlFile.exists()) {
            if (folderView != null) folderView.stopIconWorker();
            loadFolder(urlFile.getAbsoluteFile().getParentFile());
            folder.setCurrent(urlFile);
            folderView.setSelectedItem(folder.selected);
            if (imagePanel == null || !imagePanel.getPath().equals(urlFile)) {
                if (imagePanel != null) contentsPanel.remove(imagePanel);
                imagePanel = new ImagePanel(urlFile);
                contentsPanel.add(imagePanel, IMAGE_PANE);
            }
            contentsLayout.show(contentsPanel, IMAGE_PANE);
            currentCard = IMAGE_PANE;
            setTmpToolBarComponents(imagePanel.getToolBarItems());
        } else {
            urlBar.setText((currentFile != null) ? currentFile.getPath() : "");
            return;
        }
        currentFile = urlFile;
        frame.setTitle("JimgView - " + urlFile.getName());
        frame.revalidate();
    }

    /**
     * Load directory as the current directory. If the path is equal to path of
     * the currently loaded directory, function doesn't do anything.
     * @param file The directory to load.
     */
    private static void loadFolder(File file) {
        if (folder == null || !file.equals(folder.path)) {
            folder = new Folder(file);
            if (folderView != null) contentsPanel.remove(folderView);
            folderView = new FolderView(folder);
            contentsLayout.addLayoutComponent(folderView, FOLDER_PANE);
            contentsPanel.add(folderView, FOLDER_PANE);
        }
    }

    /**
     * Function event/shortcut handling function. The shortcuts should work from
     * anywhere in the application, not just when the inner component is focused.
     * This function should handle that and forward the event to the active window.
     * @param e The key event.
     */
    private static void keyPressed(KeyEvent e) {
        Action action = Settings.getKeyAction(e);
        if (action != null) {
            switch (currentCard) {
                case FOLDER_PANE: folderView.onAction(action); break;
                case IMAGE_PANE:  imagePanel.onAction(action); break;
            }
        }
    }

    /**
     * Sets temporary toolbar components. Folder view and image view have different
     * menu items. This removes any current temporary menu items and adds new ones.
     * @param components The components to add to main application tool bar.
     */
    private static void setTmpToolBarComponents(JComponent components[]) {
        if (tmpToolBarComponents != null) for (JComponent c: tmpToolBarComponents) {
            toolBar.remove(c);
        }
        tmpToolBarComponents = components;
        if (tmpToolBarComponents != null) for (JComponent c: tmpToolBarComponents) {
            toolBar.add(c);
        }
    }

    /**
     * Creates default application menu bar, setting event handers, shortcuts, ...
     * @return JMenuBar which is to be used as the application's manu bar.
     */
    private static JMenuBar createMenuBar() {
        JMenuBar ret = new JMenuBar();
        
        /*
         * Mnemonics are set only for top level JMenu, it's easy enough to move
         * through the menu with arrow keys/enter. The mnemonics are set with
         * "obsolete" function using chars (instead of key codes), which makes
         * it possible to use first letter of the text for the shortcut
         * regardless of language.
         */
        
        /* File */
        String fileText = L10n.getString("File");
        JMenu menuFile = new JMenu(fileText);
        menuFile.setMnemonic(Character.toLowerCase(fileText.charAt(0)));
        JMenuItem menuFileOpen = new JMenuItem(L10n.getString("Open"));
        JMenuItem menuFileExit = new JMenuItem(L10n.getString("Quit"));

        menuFileOpen.addActionListener(e -> open());
        menuFileExit.addActionListener(e -> {
            Settings.savePreferences();
            System.exit(0);
        });

        menuFile.add(menuFileOpen);
        menuFile.add(menuFileExit);
        ret.add(menuFile);

        /* View */
        String viewText = L10n.getString("View");
        JMenu menuView = new JMenu(viewText);
        menuView.setMnemonic(Character.toLowerCase(viewText.charAt(0)));
        
        /* Interpolation selection dialog */
        JMenu menuViewImageScaling = new JMenu(L10n.getString("ImageScaling"));
        JMenu menuViewIconScaling = new JMenu(L10n.getString("IconScaling"));
        ButtonGroup imageScalingGroup = new ButtonGroup();
        ButtonGroup iconScalingGroup = new ButtonGroup();
        for (String key: Settings.interpolationMethods.keySet()) {
            JRadioButtonMenuItem itemIcon  = new JRadioButtonMenuItem(L10n.getString(key));
            JRadioButtonMenuItem itemImage = new JRadioButtonMenuItem(L10n.getString(key));
            itemImage.addActionListener((e) -> {
                Settings.setImageScaleMethod(key);
                frame.repaint();
            });
            itemIcon.addActionListener((e) -> Settings.setIconScaleMethod(key));
            if (key.equals(Settings.imageScaleMethodName)) itemImage.setSelected(true);
            if (key.equals(Settings.iconScaleMethodName)) itemIcon.setSelected(true);
            imageScalingGroup.add(itemImage);
            iconScalingGroup.add(itemIcon);
            menuViewImageScaling.add(itemImage);
            menuViewIconScaling.add(itemIcon);
        }
        menuView.add(menuViewImageScaling);
        menuView.add(menuViewIconScaling);
        
        JMenuItem menuViewClearCache = new JMenuItem(L10n.getString("ClearIconCache"));
        menuViewClearCache.setToolTipText(L10n.getString("ClearIconCacheDesc"));
        menuViewClearCache.addActionListener(e -> ThumbManager.clearCache());
        
        menuView.add(menuViewClearCache);

        ret.add(menuView);

        return ret;
    }

    /**
     * Function which creates the main window's url bar, setting event handlers and such...
     * @param url If not null, the url bar will contain this url.
     * @return Swing component that's to be used as url bar.
     */
    private static JTextField createUrlBar(String url) {
        JTextField ret = new JTextField();
        ret.setMargin(new Insets(2, 0, 2, 0));
        ret.addActionListener(e -> {
            urlLoad();
            frame.requestFocus();
        });
        ret.setText(url != null ? url : "");
        ret.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyChar() == KeyEvent.VK_ESCAPE) {
                    frame.requestFocus();
                }
            }
        });
        return ret;
    }
}
