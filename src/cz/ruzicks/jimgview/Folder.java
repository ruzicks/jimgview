package cz.ruzicks.jimgview;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * Logical representation of a directory that keeps list of files, of currently
 * "selected" file, and has interface for going to next/previous file/directory.
 * The operations can also be selectively performed on all files/directories,
 * or on (image) files only.
 */
public class Folder {
    /** Files in the folder. Sorted so that directories are at the beginning. */
    public List<File> files;
    /** index of currently selected file in {@link #files} */
    public int selected = -1;
    /** path to the folder */
    public File path;
    /** first non-directory entry in {@link #files} */
    private int directoriesEnd;

    /**
     * Construct a folder from provided path.
     * @param path The directory to read. Should be a valid directory.
     */
    public Folder(File path) {
        this.path = path;
        FileNameExtensionFilter extFilter = new FileNameExtensionFilter(
                "images", "gif", "jpg", "jpeg", "png", "bmp");
        files = Arrays.asList(path.listFiles(
                f -> !f.isHidden() && (f.isDirectory() || extFilter.accept(f))));
        files.sort((a, b) -> compareFiles(a, b));
        for (directoriesEnd = 0;
                directoriesEnd < files.size() && files.get(directoriesEnd).isDirectory();
                ++ directoriesEnd);
    }

    public void setCurrent(File file) {
        int idx = Collections.binarySearch(files, file, (a, b) -> compareFiles(a, b));
        if (idx >= 0) selected = idx;
        else selected = -1;
        /*
        for (int i = 0; i < files.size(); ++i) {
            if (files.get(i).compareTo(file) == 0) {
                selected = i;
                return;
            }
        }
        selected = -1;*/
    }

    /**
     * Set the file after currently selected image file as the selected file and return it.
     * @return The newly selected file
     */
    public File nextImage() {
        if (files.size() == 0) return null;
        selected %= files.size();
        if (selected < 0) selected += files.size();
        int start = selected;
        for (int i = (selected+1)%files.size(); i != start; i = (i+1)%files.size()) {
            if (files.get(i).isDirectory()) continue;
            selected = i;
            return files.get(i);
        }
        return null;
    }

    /**
     * Set the file before currently selected image file as the selected file and return it.
     * @return The newly selected file.
     */
    public File prevImage() {
        if (files.size() == 0) return null;
        selected %= files.size();
        if (selected < 0) selected += files.size();
        int start = selected;
        for (int i = (selected-1)%files.size(); i != start; i = (i-1)%files.size()) {
            if (i < 0) i += files.size();
            if (files.get(i).isDirectory()) continue;
            selected = i;
            return files.get(i);
        }
        return null;
    }

    /**
     * Set the file after currently selected image file as the selected file and return
     * its index. Can return directories too.
     * @return The newly selected file
     */
    public Integer next() {
        if (files.size() == 0) return null;
        selected %= files.size();
        if (selected < 0) selected += files.size();
        int start = selected;
        for (int i = (selected+1)%files.size(); i != start; i = (i+1)%files.size()) {
            selected = i;
            return i;
        }
        return null;
    }

    /**
     * Set the file before currently selected image file as the selected file and return
     * its index. Can return directories too.
     * @return The newly selected file.
     */
    public Integer prev() {
        if (files.size() == 0) return null;
        selected %= files.size();
        if (selected < 0) selected += files.size();
        int start = selected;
        for (int i = (selected-1)%files.size(); i != start; i = (i-1)%files.size()) {
            if (i < 0) i += files.size();
            selected = i;
            return i;
        }
        return null;
    }


    /**
     * Get index of selected file, only taking images into account.
     * Example: there are 4 directories, 12 files total. Then if the last file
     * is the selected one, its index will be 8
     * @return index of the current image
     */
    public int getSelectedImageIdx() {
        if (selected == -1 || files.get(selected).isDirectory()) return 0;
        return selected + 1 - directoriesEnd;
    }

    /**
     * Get total number of images.
     * @return Number of images in the folder.
     */
    public int getImagesTotal() {
        return files.size() - directoriesEnd;
    }


    /**
     * Compare files according to directory view sort order.
     * Directories are first, rest is handled by {@link File#compareTo}.
     * @param a First file for comparison.
     * @param b Second file for comparison.
     * @return Value less than zero if a comes before b, 0 if files are equal,
     *     value greater than zero if a comes after b.
     */
    public static int compareFiles(File a, File b) {
        if ( a.isDirectory() && !b.isDirectory()) return -1;
        if (!a.isDirectory() &&  b.isDirectory()) return 1;
        return a.compareTo(b);
    }

    /**
     * Returns number of files in directory
     * @return Number of files in directory.
     */
    public int size() {
        return files.size();
    }
}
