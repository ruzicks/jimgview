/**
 * Contains classes for the Jimgview program. The main class and other classes
 * except swing components are directly in this package, swing components are in
 * <code>components</code> sub-package.
 */
package cz.ruzicks.jimgview;