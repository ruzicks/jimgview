
package cz.ruzicks.jimgview;

import java.io.UnsupportedEncodingException;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Small wrapper around {@link ResourceBundle} for retrieving translation
 * messages.
 */
public class L10n {
    /**
     * Resource bundle with translation messages.
     * Note: the messages are stored as UTF8, but java {@link ResourceBundle}
     * expects ISO-8859-1, so the messages have to be converted.
     * (<code>L10n.getString</code> takes care of that).
     */
    private static ResourceBundle messages = ResourceBundle.getBundle(
        "cz.ruzicks.jimgview.resources.messages");
    
    /**
     * Get message for specified message string.
     * @param name Name of the message.
     * @return Returns the requested message, or <code>null</code> on failure.
     */
    public static String getString(String name) {
        String ret = null;
        try {
            ret = messages.getString(name);
            return new String(ret.getBytes("ISO-8859-1"), "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(L10n.class.getName()).log(Level.SEVERE, null, ex);
            return ret;
        } catch (MissingResourceException ex) {
            Logger.getLogger(L10n.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
	}
}
