/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.ruzicks.jimgview;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;


/**
 * Thumbnail manager for generating and caching thumbnails/previews.
 */
public class ThumbManager {
    /** The cached previews */
    private static HashMap<String, CachedThumbnail> thumbs = new HashMap<>();

    /**
     * Get preview for specified path. Uses globally set preview size.
     * @param path Path to the image
     * @return Down-scaled <code>ImageIcon</code> of the image, null if path is
     *     not a valid image.
     */
    public static synchronized ImageIcon getIcon(File path) {
        return getIcon(path, Settings.getIconWidth(), Settings.getIconHeight());
    }

    /**
     * Get preview for specified path, with specified maximum width/height.
     * @param path Path to the image.
     * @param width Maximum width.
     * @param height Maximum height.
     * @return The preview icon. If path isn't valid image, returns null.
     */
    public static synchronized ImageIcon getIcon(File path, int width, int height) {
        CachedThumbnail ret = thumbs.get(path.getAbsolutePath());
        if (ret != null) return ret.getThumb(width, height);
        else {
            ret = new CachedThumbnail(path);
            thumbs.put(path.getAbsolutePath(), ret);
            return ret.getThumb(width, height);
        }
    }

    /**
     * Clears cached thumbnails.
     */
    public static synchronized void clearCache() {
        thumbs.clear();
    }

    /**
     * Class representing a chached thumbnail. Takes care of (re)loading and
     * scaling the image or downscaling the current preview.
     */
    private static class CachedThumbnail {
        private File path;
        private ImageIcon thumb = null;

        /** File lengh. It desireable to regenerate the icon if the file changed */
        private long length = 0;
        /** current preview width */
        private int width = 0;
        /** current preview height */
        private int height = 0;

        /** true when there was an error while loading the image */
        private boolean err = false;

        /**
         * Create cached thumbnail from specified path. The image is not
         * actually loaded and scaled until {@link #getThumb(int, int) } is called
         * @param path Path to the image.
         */
        public CachedThumbnail(File path) {
            this.path = path;
        }

        /**
         * Generate and return icon for this cached thumbnail.
         * @param w Maximum width.
         * @param h Maximum height.
         * @return the image thumbnail, or null if reading image failed
         */
        public ImageIcon getThumb(int w, int h) {
            if (err) return null;
            if (length != path.length() || (w > width+1 && h > height+1)) {
                length = path.length();
                BufferedImage img;
                try {
                    img = ImageIO.read(path);
                    if (img == null) return null;
                } catch (IOException|ArrayIndexOutOfBoundsException ex) {
                    /* some image readers were throwing arr. out of bounds ex. too */
                    Logger.getLogger(ThumbManager.class.getName()).log(Level.SEVERE, null, ex);
                    err = true;
                    return null;
                }
                width = img.getWidth();
                height = img.getHeight();
                if (w/(double)h > width/(double)height) {
                    width = (width*h)/height;
                    height = h;
                } else {
                    height = (height*w)/width;
                    width = w;
                }
                thumb = getIcon(img);
                // thumb = new ImageIcon(img.getScaledInstance(
                //             imgW, imgH, Settings.icon_scale_method));
            } else if (w < width || h < height) {
                if (w/(double)h > width/(double)height) {
                    width = (width*h)/height;
                    height = h;
                } else {
                    height = (height*w)/width;
                    width = w;
                }
                thumb = getIcon(thumb.getImage());
                // thumb = new ImageIcon(thumb.getImage().getScaledInstance(
                //             width, height, Settings.icon_scale_method));
            }
            return thumb;
        }

        /**
         * Get image icon with currently set size from specified image.
         * Convenience function to reduce code repetition.
         * Uses <code>Graphics2D#drawImage</code> because <code>Image#getScaledInstance</code>
         * is way slower.
         * @param src The image to scale.
         * @return <code>ImageIcon</code> with {@link #width} and {@link #height}
         */
        private ImageIcon getIcon(Image src) {
            BufferedImage ret = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = (Graphics2D)ret.createGraphics();
            RenderingHints hints = new RenderingHints(
                    RenderingHints.KEY_INTERPOLATION,
                    Settings.iconScaleMethod);
            g.addRenderingHints(hints);
            g.drawImage(src, 0, 0, width, height, null);
            g.dispose();
            return new ImageIcon(ret);
        }
    }
}

