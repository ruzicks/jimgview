package cz.ruzicks.jimgview;

import java.awt.Color;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.net.URL;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.swing.ImageIcon;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

/**
 * Class containing settings constants, global settings values, getters/setters,
 * convenience functions and manages preferences saving/loading.
 */
public class Settings {
    /** Local user configuration object */
    private static final Preferences preferences = Preferences.userNodeForPackage(Jimgview.class);

    /*
     * Prefernces keys
     */
    /** image interpolation preference key */
    private static final String CFG_IMAGE_INTERPOLATION = "image_interpolation_method";
    /** icon interpolation preference key */
    private static final String CFG_ICON_INTERPOLATION = "icon_interpolation_method";
    /** folder preview zoom preference key */
    private static final String CFG_ICON_ZOOM = "icon_zoom";

    /*
     * SETTING CONSTANTS
     */

    /** Key code shortcuts - for nonprintable keys */
    private static final HashMap<Integer, Jimgview.Action> shortcutsCodes =
        new HashMap<Integer, Jimgview.Action>() {{
            put(KeyEvent.VK_LEFT      , Jimgview.Action.GO_PREV);
            put(KeyEvent.VK_RIGHT     , Jimgview.Action.GO_NEXT);
            put(KeyEvent.VK_DOWN      , Jimgview.Action.SCROLL_DOWN);
            put(KeyEvent.VK_UP        , Jimgview.Action.SCROLL_UP);
            put(KeyEvent.VK_HOME      , Jimgview.Action.GO_UP);
            put(KeyEvent.VK_ENTER     , Jimgview.Action.ENTER);
        }};
    /**
     * Character shortcuts - bound to resulting character, rather than specific key codes.
     * For example '-' on keyboard and on numpad have different keycodes, but produce the
     * same character and  On some layouts non-numad '+' doesn't have any keycode at all.
     */
    private static final HashMap<Character, Jimgview.Action> shortcutsChars =
        new HashMap<Character, Jimgview.Action>() {{
            put('+', Jimgview.Action.ZOOM_IN);
            put('-', Jimgview.Action.ZOOM_OUT);
            put('0', Jimgview.Action.ZOOM_FIT);
            put('=', Jimgview.Action.ZOOM_FIT);
            put('1', Jimgview.Action.ZOOM_1TO1);
            put('w', Jimgview.Action.SCROLL_UP);
            put('s', Jimgview.Action.SCROLL_DOWN);
            put('a', Jimgview.Action.SCROLL_LEFT);
            put('d', Jimgview.Action.SCROLL_RIGHT);
        }};
    /** Base icon width. Resulting width is calculated using {@link #iconZoom} */
    public static final int BASE_ICON_WIDTH = 100;
    /** Base icon height. Resulting height is calculated using {@link #iconZoom} */
    public static final int BASE_ICON_HEIGHT = 100;
    /** Amount of zoom change applied in one step. */
    public static final float FOLDER_ZOOM_STEP = 0.2f;

    /** Default width of program's window. */
    public static final int DEFAULT_WINDOW_WIDTH = 400;
    /** Default height of program's window. */
    public static final int DEFAULT_WINDOW_HEIGHT = 400;

    /** Background color for folder item */
    public static final Color FOLDER_ITEM_BG_COLOR = new Color(1f, 1f, 1f, 0.3f);
    /** Border of selected item in folder view */
    public static final Border SELECTED_FILE_BORDER =
        new LineBorder(new Color(0f, 0f, 0f, 0.3f), 2);
    /** Standard border of item in folder view */
    public static final Border STANDARD_FILE_BORDER =
        new LineBorder(FOLDER_ITEM_BG_COLOR, 2);

    /*
     * RANGES/OPTIONS FOR VARIABLE VALUES
     */

    /** interpolation method hash map */
    public static final TreeMap<String, Object> interpolationMethods =
        new TreeMap<String, Object>() {{
            put("bicubic", RenderingHints.VALUE_INTERPOLATION_BICUBIC);
            put("bilinear", RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            put("nearest_neighbor", RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
        }};

    /*
     * VARIABLE SETTING VALUES
     */

    /**
     * Current folder view icon zoom. <code>1.0</code> = {@link #BASE_ICON_WIDTH}
     * and {@link #BASE_ICON_HEIGHT}
     */
    public static float iconZoom = 1f;
    
    /** Scaling method for icon generation */
    public static Object iconScaleMethod  = RenderingHints.VALUE_INTERPOLATION_BILINEAR;
    /** Name of current image scaling method. */
    public static String imageScaleMethodName = "bilinear";
    
    /** Scaling method for zooming in/out */
    public static Object imageScaleMethod = RenderingHints.VALUE_INTERPOLATION_BILINEAR;
    /** Name of current icon scaling method */
    public static String iconScaleMethodName = "bilinear";

    /**
     * Loads user preferences, if any are set
     */
    public static void loadPreferences() {
        try {
            setIconScaleMethod(preferences.get(CFG_ICON_INTERPOLATION, "bilinear"));
            setImageScaleMethod(preferences.get(CFG_IMAGE_INTERPOLATION, "bilinear"));
            iconZoom = preferences.getFloat(CFG_ICON_ZOOM, 1.0f);
            if (iconZoom < 0.05 || iconZoom > 15) iconZoom = 1;
        } catch (IllegalStateException ex) {
            Logger.getLogger(Settings.class.getName()).log(Level.SEVERE, null, ex);
            iconScaleMethod = interpolationMethods.get("bilinear");
            imageScaleMethod = interpolationMethods.get("bilinear");
            iconZoom = 1;
        }
    }
    
    /**
     * Saves current settings to local preferences storage.
     */
    public static void savePreferences() {
        try {
            preferences.put(CFG_ICON_INTERPOLATION, iconScaleMethodName);
            preferences.put(CFG_IMAGE_INTERPOLATION, imageScaleMethodName);
            if (iconZoom >= 0.05 && iconZoom < 15) {
                preferences.putFloat(CFG_ICON_ZOOM, iconZoom);
            }
            preferences.flush();
        } catch (IllegalStateException|BackingStoreException ex) {
            Logger.getLogger(Settings.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Sets image interpolation method.
     * @param name Name of the interpolation method (see {@link #interpolationMethods}).
     *     If name is not valid, this the function doesn't do anything.
     */
    public static void setImageScaleMethod(String name) {
        Object method = interpolationMethods.get(name);
        if (method == null) return;
        imageScaleMethod = method;
        imageScaleMethodName = name;
    }
    
    /**
     * Sets icon interpolation method.
     * @param name Name of the interpolation method (see {@link #interpolationMethods}).
     *     If name is not valid, this the function doesn't do anything.
     */
    public static void setIconScaleMethod(String name) {
        Object method = interpolationMethods.get(name);
        if (method == null) return;
        iconScaleMethod = method;
        iconScaleMethodName = name;
    }
    
    /**
     * Get action corresponding to specified key event.
     * @param e The key event.
     * @return The action constant, or null if the key event wasn't bound to anything.
     */
    public static Jimgview.Action getKeyAction(KeyEvent e) {
        Jimgview.Action ret = shortcutsCodes.get(e.getKeyCode());
        if (ret != null) return ret;
        return shortcutsChars.get(e.getKeyChar());
    }

    /**
     * Get icon for specified action. Looks for the icon (only) amoung bundled icons.
     * @param name Name of the icon. Icons can be found/placed into
     *     cz/ruzicks/jimgview/resources/ and are expected to have .png extension.
     * @return Icon corresponding to specified name or null if it doesn't exist.
     */
    public static ImageIcon getIconResource(String name) {
        if (name == null) return null;
        URL res = Settings.class.getClassLoader().getResource(
                "cz/ruzicks/jimgview/resources/" + name + ".png");
        if (res != null) return new ImageIcon(res);
        else return null;
    }

    /**
     * Returns current icon width for folder view
     * @return width in pixels
     */
    public static int getIconWidth() {
        return (int)(BASE_ICON_WIDTH * Settings.iconZoom);
    }

    /**
     * Returns current icon height for folder view
     * @return height in pixels
     */
    public static int getIconHeight() {
        return (int)(BASE_ICON_HEIGHT * Settings.iconZoom);
    }

    /**
     * Changes icon zoom by <i>amount</i> steps.
     * @param amount determines how much zoom is added or subtracted.
     */
    public static void iconZoomAdd(int amount) {
        iconZoom += amount*FOLDER_ZOOM_STEP;
        if (iconZoom < 0.05f) iconZoom = 0.05f;
    }
}
