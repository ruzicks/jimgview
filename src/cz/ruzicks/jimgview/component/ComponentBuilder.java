package cz.ruzicks.jimgview.component;

import java.awt.Insets;
import java.awt.event.ActionListener;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import cz.ruzicks.jimgview.L10n;
import cz.ruzicks.jimgview.Settings;

/**
 * Convenience class for building component using local resources (text, icons)
 */
public class ComponentBuilder {

    /**
     * Create button, with name and tooltip according to name, and appropriate
     * icon. Sets only icon if available, text otherwise
     * @param name Name of the button. The relevant resource file records which
     *     determine the text and description displayed are
     *     <code>btn_text_{name}</code> and <code>btn_desc_{name}</code>. If no
     *     icon or text resource is available, the <code>name</code> is used as
     *     the button text instead.
     * @param iconName Name of the icon. Taken from resources folder.
     * @param action Action passed to {@link JButton#addActionListener}.
     * @return The resulting button.
     */
    public static JButton jButton(String name, String iconName, ActionListener action) {
        JButton ret = new JButton();
        ret.setMargin(new Insets(0, 0, 0, 0));
        ret.setFocusable(false);
        ret.addActionListener(action);

        ImageIcon icon = Settings.getIconResource(iconName);

        if (icon != null) {
            ret.setIcon(icon);
        } else {
            String text = L10n.getString("btn_text_" + name);
            ret.setText(text != null ? text : name);
        }

        String desc = L10n.getString("btn_desc_" + name);
        ret.setToolTipText(desc != null ? desc : name);
        return ret;
    }
}
