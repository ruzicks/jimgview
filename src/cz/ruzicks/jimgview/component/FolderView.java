package cz.ruzicks.jimgview.component;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.border.LineBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.wordpress.tips4java.WrapLayout;
import cz.ruzicks.jimgview.Folder;
import cz.ruzicks.jimgview.Jimgview;
import cz.ruzicks.jimgview.L10n;
import cz.ruzicks.jimgview.Settings;
import cz.ruzicks.jimgview.ThumbManager;
import java.awt.Rectangle;


/**
 * Swing component represeting a folder view, showing directories and images.
 * View is created from instance of {@link cz.ruzicks.jimgview.Folder Folder}
 * and is only meant to take care of the graphical part. In order to have icons, the icon
 * worker has to be started with {@link #startIconWorker}.
 * */
public class FolderView extends JScrollPane implements MouseWheelListener {
    private JPanel innerPane;
    private ArrayList<FolderItem> items;
    private int selected = -1;
    private IconWorker worker = null;
    private Folder folder;

    /**
     * Create swing component for specified folder
     * @param folder The folder to make view of.
     */
    public FolderView(Folder folder) {
        super(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        super.getVerticalScrollBar().setUnitIncrement(16);
        setFocusable(true);
        innerPane = new JPanel(new WrapLayout(FlowLayout.LEFT));
        super.setViewportView(innerPane);

        this.folder = folder;
        items = new ArrayList<>(folder.files.size());
        for (int i = 0; i<folder.files.size(); ++i) {
            FolderItem fi = new FolderItem(folder.files.get(i), i);
            items.add(fi);
            innerPane.add(fi);
        }
        super.addMouseWheelListener(this);
    }

    /**
     * Sets the {@link FolderItem} as the selected one and deselects the previously
     * selected one.
     * @param idx Index of the newly selected item.
     */
    public void setSelectedItem(int idx) {
        if (idx < 0 || idx >= items.size()) return;
        if (selected != -1) {
            items.get(selected).setSelected(false);
        }
        selected = idx;
        FolderItem item = items.get(selected);
        item.setSelected(true);
        scrollToSelected();
        repaint();
    }

    /**
     * Return path to the displayed folder.
     * @return Path the the displayed folder.
     */
    public File getPath() { return folder.path; }

    /**
     * Refresh display size of {@link FolderItem}s
     */
    public void zoomChanged() {
        for (FolderItem fi: items) {
            fi.mSetSize(Settings.getIconWidth(), Settings.getIconWidth());
        }
        super.revalidate();
        if (worker != null) worker.isVisible = false;
        worker = new IconWorker();
        worker.execute();
        scrollToSelected();
    }

    /**
     * Start generating icons for images in the folder
     */
    public void startIconWorker() {
        if (worker != null) worker.isVisible = false;
        worker = new IconWorker();
        worker.execute();
    }

    /**
     * Stop generating icons for images in the folder
     */
    public void stopIconWorker() {
        if (worker != null) worker.isVisible = false;
    }

    /**
     * Ensure that {@link FolderView} doesn't keep generating icons after it is
     * removed from the hierarchy. Propagates the call to
     * {@link JScrollPane#removeNotify}.
     */
    @Override
    public void removeNotify() { stopIconWorker(); super.removeNotify(); }

    /**
     * Use CTRL+mousewheel for zoom in/out. Propagates the event if CTRL wasn't pressed.
     * @param evt The mouse wheel event.
     */
    @Override
    public void mouseWheelMoved(MouseWheelEvent evt) {
        if ((evt.getModifiersEx() & MouseWheelEvent.CTRL_DOWN_MASK) != 0) {
            /* negate, so that scroll up zooms and down shrinks */
            int amount = -evt.getWheelRotation();
            if (amount == 0) return;
            Settings.iconZoomAdd(amount);
            zoomChanged();
        } else if (getParent() != null){
            getParent().dispatchEvent(SwingUtilities.convertMouseEvent(this, evt, getParent()));
        }
    }

    /**
     * Action handler for global keybinds.
     * @param a The action to handle.
     */
    public void onAction(Jimgview.Action a) {
        switch (a) {
        case GO_UP:    Jimgview.loadPath(folder.path.getParentFile()); break;
        case ZOOM_IN:  Settings.iconZoomAdd(1);  zoomChanged(); break;
        case ZOOM_OUT: Settings.iconZoomAdd(-1); zoomChanged(); break;
        case GO_NEXT:  setSelectedItem(selected + 1); break;
        case GO_PREV:  setSelectedItem(selected - 1); break;
        case ENTER:
            if (selected < 0 || selected > items.size()) break;
            folder.selected = selected;
            Jimgview.loadPath(items.get(selected).path);
            break;
        case SCROLL_UP:
            getVerticalScrollBar().setValue(getVerticalScrollBar().getValue()-16);
            break;
        case SCROLL_DOWN:
            getVerticalScrollBar().setValue(getVerticalScrollBar().getValue()+16);
            break;
        }
    }

    /**
     * Returns components to be displayed in a tool bar. Currently returns
     * zoom in/out buttons and "up" button.
     * @return Components to be added to a tool bar.
     */
    public JComponent[] getToolBarItems() {
        return new JComponent[] {
            ComponentBuilder.jButton("up", "action_up", e -> {
                Jimgview.loadPath(folder.path.getParentFile());
            }),
            ComponentBuilder.jButton("zoom_plus", "action_plus", e -> {
                Settings.iconZoomAdd(1);
                zoomChanged();
            }),
            ComponentBuilder.jButton("zoom_minus", "action_minus", e -> {
                Settings.iconZoomAdd(-1);
                zoomChanged();
            })
        };
    }

    /**
     * If selected item isn't visible, scrolls to it.
     */
    private void scrollToSelected() {
        if (selected < 0 || selected >= items.size()) return;
        /*
         * Scroll to correct position. scrollRectToVisible doesn't work properly
         * for some reason, so I manually calculate visibility and scroll to
         * the right position.
         */
        Rectangle itemBounds = items.get(selected).getBounds();
        Dimension viewportSize = getViewport().getSize();
        int min = getVerticalScrollBar().getValue();
        int max = min+viewportSize.height;
        if (itemBounds.y < min) getVerticalScrollBar().setValue(itemBounds.y);
        else if (itemBounds.y + itemBounds.height > max) {
            getVerticalScrollBar().setValue(
                    itemBounds.y + itemBounds.height - viewportSize.height);
        }
    }

    /**
     * Worker for asynchronously generating and displaying icons. Relies on
     * {@link cz.ruzicks.jimgview.ThumbManager ThumbManager} for caching.
     */
    private class IconWorker extends SwingWorker<Integer, FolderItem> {
        public boolean isVisible = true;
        @Override
        protected Integer doInBackground() {
            for (FolderItem fi: items) {
                if (!isVisible) return null;
                fi.mSetSize(Settings.getIconWidth(), Settings.getIconHeight());
                fi.prepareResources();
                publish(fi);
            }
            return null;
        }
        @Override
        protected void process(List<FolderItem> preparedItems) {
            for (FolderItem fi: preparedItems) {
                if (!isVisible) return;
                fi.display();
                innerPane.revalidate();
                innerPane.repaint();
            }
        }
    }

    /**
     * A component representing a file in the folder view
     */
    private class FolderItem extends JLabel {
        private File path;
        private int width;
        private int height;
        private ImageIcon icon = null;
        private String text = null;
        private int idx;
        private boolean isSelected;

        /**
         * Create a folder item component for specified file and index.
         * Displays file name. To display icon, {@link #prepareResources} has
         * and then {@link #display} have to be called.
         * @param path Path to the file.
         * @param idx Index of the file in the currently loaded folder.
         */
        public FolderItem(File path, int idx) {
            this.path = path;
            this.idx = idx;
            this.height = Settings.getIconHeight();
            this.width = Settings.getIconWidth();
            //JLabel label = new JLabel();

            mSetSize(new Dimension(width, height));
            setOpaque(true);
            setBackground(Settings.FOLDER_ITEM_BG_COLOR);
            Font oldFont = getFont();
            setFont(new Font(oldFont.getName(), Font.PLAIN, 10));
            setHorizontalAlignment(SwingConstants.CENTER);
            text = path.getName();
            display();
            addMouseListener((MouseClickedListener)(e) -> {
                folder.selected = idx;
                Jimgview.loadPath(this.path);
            });
        }

        /**
         * Do operations could potentially last longer. Currently gets the icon.
         */
        public void prepareResources() {
            if (path.isDirectory()) return;
            icon = ThumbManager.getIcon(path, width, height);
        }

        /**
         * Display the item with the currently available resources. Only
         * displays name if icon isn't prepared
         */
        public void display() {
            if (path.isDirectory()) {
                setText(text);
                if (icon != null) setIcon(icon);
            } else {
                if (icon != null) {
                    super.setText(null);
                    setIcon(icon);
                }
                else setText(text);
            }
            if (isSelected) {
                super.setBorder(Settings.SELECTED_FILE_BORDER);
            } else super.setBorder(Settings.STANDARD_FILE_BORDER);
        }

        /**
         * Set display size of item
         * @param dim The display size.
         */
        public void mSetSize(Dimension dim) {
            width = dim.width;
            height = dim.height;
            setSize(dim);
            setMinimumSize(dim);
            setPreferredSize(dim);
            setMaximumSize(dim);
        }

        /**
         * Sets whether to display item as selected.
         * @param isSelected <code>true</code> to set as selected, <code>false</code> to
         *     set as not selected.
         */
        public void setSelected(boolean isSelected) {
            if (this.isSelected == isSelected) return;
            this.isSelected = isSelected;
            display();
            revalidate();
        }
        /**
         * Set display size of item
         * @param w Display width.
         * @param h Display height.
         */
        public void mSetSize(int w, int h) {
            mSetSize(new Dimension(w, h));
        }
    }
}

/** Convenience interface to make creating simple click listeners easier */
interface MouseClickedListener extends MouseListener {
    @Override public default void mouseEntered(MouseEvent e) {}
    @Override public default void mouseExited(MouseEvent e) {}
    @Override public default void mousePressed(MouseEvent e) {}
    @Override public default void mouseReleased(MouseEvent e) {}
}
