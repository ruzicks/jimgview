package cz.ruzicks.jimgview.component;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import cz.ruzicks.jimgview.L10n;
import cz.ruzicks.jimgview.Settings;

/**
 * The image view swing component. The component is a {@link JScrollPane}
 * containing the image. The component supports scroll-wheel zooming in/out,
 * drag-scrolling.
 */
public class ImageView extends JScrollPane {
    /** The File which is being displayed. */
    private File path;

    /** The image, or <code>null</code> if {@link #path} isn't valid image. */
    private BufferedImage image = null;

    /** Error message, when {@link #path} isn't a valid image), or <code>null</code> */
    private String text = null;

    /**
     * Value determining whether the image should be scaled so that it fits
     * inside the visible rectangle, <code>true</code> by default.
     */
    private boolean fitInWindow = true;

    /**
     * The component inside this <code>JScrollPane</code>. Either
     * {@link InnerImageView}, or <code>JPanel</code> containing an error
     * message.
     */
    private JComponent innerPanel;

    /**
     * Observers for receiving information about the <code>ImageView</code>.
     * Currently being used for status line (showing zoom)
     */
    private ArrayList<ImageViewObserver> observers = new ArrayList<>();

    /**
     * Create component showing specified image in a scrollPane.
     * @param path the image to display
     */
    public ImageView(File path) {
        super();
        super.getVerticalScrollBar().setUnitIncrement(16);
        assert(path.isFile());
        this.path = path;
        try {
            image = ImageIO.read(path);
        } catch (IOException|ArrayIndexOutOfBoundsException ex) {
            Logger.getLogger(ImageView.class.getName()).log(Level.SEVERE, null, ex);
            image = null;
        }
        if (image == null) {
            /* to have the image centered; */
             innerPanel = new JPanel(new GridBagLayout());
             innerPanel.add(
                     new JLabel(L10n.getString("image_loading_failed")),
                     new GridBagConstraints());
        } else {
            innerPanel = new InnerImageView();
        }
        super.setViewportView(innerPanel);
        super.revalidate();
        innerPanel.repaint();
    }

    /**
     * Set zoom of the contained image.
     * @param zoom The new value, 1.0 = 100%
     */
    public void setZoom(double zoom) {
        if (innerPanel instanceof InnerImageView) {
            ((InnerImageView)innerPanel).changeZoom(zoom, null, null);
        }
    }

    /**
     * Get current zoom of the contained image.
     * @return the zoom value, 1.0 = 100%, <code>null</code> if {@link #path}
     *         isn't a valid image
     */
    public Double getZoom() {
        if (innerPanel instanceof InnerImageView) {
            return ((InnerImageView)innerPanel).zoom;
        } else return null;
    }

    /**
     * Get size of the displayed image
     * @return size of the image, <code>null</code> if {@link #path} is not valid image
     */
    public Dimension getImageSize() {
        if (image != null) return new Dimension(image.getWidth(), image.getHeight());
        else return null;
    }

    /**
     * Returns File object of the image which is currently being displayed
     * @return the image File (not guaranteed to be valid path)
     */
    public File getPath() { return path; }

    /**
     * Registers the ImageViewObserver for receiving events from this ImageView.
     * @param observer Observer to add.
     */
    public void addObserver(ImageViewObserver observer) {
        observers.add(observer);
    }

    /**
     * Unregisters the ImageViewOBserver.
     * @param observer Observer to remove.
     */
    public void removeObserver(ImageViewObserver observer) {
        observers.remove(observer);
    }

    /**
     * Sets whether to always fit the image inside the window
     * @param val <code>true</code> sets the value and immetiately recalculates the size,
     *            <code>false</code> disables autofit
     */
    public void setFitInWindow(boolean val) {
        if (val == fitInWindow) return;
        fitInWindow = val;
        if (fitInWindow && innerPanel instanceof InnerImageView) {
            ((InnerImageView)innerPanel).recalculateSize();
            revalidate();
            repaint();
        }
    }

    /** Interface for observing changes in this ImageView */
    public interface ImageViewObserver {
        /**
         * Called whenever zoom value changes
         * @param zoom the new zoom value
         */
        public void onZoomChange(double zoom);
    }

    /**
     * Component inside the <code>JScrollPane</code>, which takes care of
     * rendering the image and user interaction (zooming, drag-scrolling...).
     */
    private class InnerImageView extends JPanel implements MouseWheelListener {
        /** current size of displayed image (accounting for zoom) */
        private Dimension size = null;
        /** size of free space between left panel border and left image border */
        private int xOffset = 0;
        /** size of free space between top panel border and top image border */
        private int yOffset = 0;
        /** current zoom of displayed image, 1.0 = 100% */
        public double zoom = 1.0;
        /** mouse listener for dragging/scrolling */
        private ImgMouseListener mouseListener;

        /** does basic initialization, registers listeners… */
        public InnerImageView() {
            setOpaque(true);
            addMouseWheelListener(this);
            mouseListener = new ImgMouseListener();
            this.addMouseListener(mouseListener);
            this.addMouseMotionListener(mouseListener);
        }

        /**
         * Paints the image on the component. Notices when image was resized,
         * or when other condition have changed and recalculates the size
         * and position.
         * @param g The graphics to paint on.
         */
        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            if (size == null) {
                recalculateSize();
            }
            Dimension currSize = getViewport().getSize();
            if (fitInWindow) recalculateSize();
            xOffset = 0;
            yOffset = 0;
            if (currSize.width  > size.width ) xOffset = (currSize.width  - size.width ) / 2;
            if (currSize.height > size.height) yOffset = (currSize.height - size.height) / 2;

            ((Graphics2D)g).addRenderingHints(new RenderingHints(
                    RenderingHints.KEY_INTERPOLATION, Settings.imageScaleMethod));
            boolean ret = g.drawImage(image,
                    xOffset, yOffset, xOffset+size.width, yOffset+size.height,
                    0, 0, image.getWidth(), image.getHeight(),
                    Color.green, null);
        }

        /**
         * Change zoom value and update the view, default center of zoom is
         * …the center.
         * @param newZoom new zoom value, 1.0 = 100%
         * @param centerX null or X component of position on the image to zoom towards/from
         * @param centerY null or Y component of position on the image to zoom towards/from
         */
        public void changeZoom(double newZoom, Double centerX, Double centerY) {
            /* stop drag-scrolling - zoom could mess up positions */
            mouseListener.origin = null;

            Dimension visibleSize = getViewport().getSize();
            /*
             * Corners of visible rectangle and the center of zoom (mouse position).
             * Translated to the actual image size.
             */
            double x1 = (getHorizontalScrollBar().getValue())/zoom;
            double y1 = (getVerticalScrollBar().getValue()  )/zoom;
            double x2 = ((getHorizontalScrollBar().getValue()+visibleSize.width -xOffset))/zoom;
            double y2 = ((getVerticalScrollBar()  .getValue()+visibleSize.height-yOffset))/zoom;
            double x =  (centerX != null) ? centerX : (x1+x2)/2;
            double y =  (centerY != null) ? centerY : (y1+y2)/2;

            zoom = newZoom;
            size.width  = (int)(image.getWidth() *zoom);
            size.height = (int)(image.getHeight()*zoom);

            /* translate points to the new size */
            x1 = (x1*zoom);
            y1 = (y1*zoom);
            x2 = (x2*zoom);
            y2 = (y2*zoom);
            x =  (x *zoom);
            y =  (y *zoom);
            /* width/height of the part that will (dis)appear */
            double extraW = x2 - x1 - visibleSize.width;
            double extraH = y2 - y1 - visibleSize.height;

            /*
             * New upper left corner of the visible rectangle.
             * Moved up with direct proportion to the distance between the corner
             * and the "zoom center" (relative the distance between both corners)
             */
            x1 += (extraW * (x-x1))/(x2-x1);
            y1 += (extraH * (y-y1))/(y2-y1);

            fitInWindow = false;
            setPreferredSize(size);
            revalidate();
            repaint();

            scrollRectToVisible(new Rectangle(
                        (int)x1, (int)y1, visibleSize.width, visibleSize.height));
            for (ImageViewObserver o: observers) {
                o.onZoomChange(zoom);
            }
        }

        /**
         * Increase/decrease zoom when mouse wheel moves up/down; zooms
         * towards/from mouse pointer.
         * @param evt the mouse wheel event
         */
        @Override
        public void mouseWheelMoved(MouseWheelEvent evt) {
            int amount = -evt.getWheelRotation();
            if (amount == 0) return;

            double zoomDiff = zoom*(0.05*amount);
            double newZoom = (zoom+zoomDiff > 0) ? zoom+zoomDiff : zoom/2;

            /* zoom towards/from the pointer position */
            changeZoom(newZoom, (evt.getX()-xOffset)/zoom, (evt.getY()-yOffset)/zoom);
        }

        /**
         * Recalculate image size to match the visible rectangle (with max zoom
         * 1.0)
         */
        public void recalculateSize() {
            size = getViewport().getSize();
            int w = image.getWidth();
            int h = image.getHeight();
            if (w > size.width) {
                h = (h*size.width)/w;
                w = size.width;
            }
            if (h > size.height) {
                w = (w*size.height)/h;
                h = size.height;
            }
            size.setSize(w, h);
            setPreferredSize(size);
            zoom = w/(double)image.getWidth();
            for (ImageViewObserver o: observers) {
                o.onZoomChange(zoom);
            }
        }

        /**
         * Mouse listener for drag-scrolling. The current action can be stopped
         * by setting the {@link ImgMouseListener#origin} to <code>null</code>.
         */
        private class ImgMouseListener extends MouseAdapter {
            /**
             * Start point of dragging/scrolling, setting to <code>null</code>
             * stops the current action.
             */
            public Point origin = null;

            /**
             * Sets the beginning point of the drag
             * @param e the triggering mouse event
             */
            @Override
            public void mousePressed(MouseEvent e) {
                origin = new Point(e.getPoint());
            }

            /**
             * Removes the beginning point
             * @param e the triggering mouse event
             */
            @Override
            public void mouseReleased(MouseEvent e) {
                origin = null;
            }

            /**
             * Scrolls the pane so that it seems as if the image was being dragged.
             * Uses {@link #origin} as reference point.
             * @param e the triggering mouse event
             */
            @Override
            public void mouseDragged(MouseEvent e) {
                if (origin == null) return;
                Rectangle rect = ImageView.this.getViewport().getViewRect();
                if (rect.width < size.width) {
                    rect.x += origin.x - e.getX();
                }
                if (rect.height < size.height) {
                    rect.y += origin.y - e.getY();
                }
                scrollRectToVisible(rect);
            }
        }
    }
}
