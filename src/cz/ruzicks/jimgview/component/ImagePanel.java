package cz.ruzicks.jimgview.component;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.io.File;
import java.text.DecimalFormat;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import cz.ruzicks.jimgview.Jimgview;
import javax.swing.JScrollBar;

/**
 * Higher level component around {@link ImageView}. It provides status bar,
 * tool bar items (it does not display this, the main window has one tool bar
 * which displays all control components together, to save space).
 */
public class ImagePanel extends JPanel {
    /** The component containing the image (or error message) */
    private ImageView imageView;
    /** Simple component displaying size, zoom, image number/#images total */
    private StatusBar statusBar;
    /** Observer for updating the status bar */
    private ImageView.ImageViewObserver observer;

    /**
     * Creates the image panel component.
     * @param path path to the image. Does not have to be valid image file,
     *     error message will be shown in such case.
     */
    public ImagePanel(File path) {
        super(new BorderLayout());
        setFocusable(true);
        imageView = new ImageView(path);
        add(imageView, BorderLayout.CENTER);
        statusBar = new StatusBar(imageView.getZoom(), imageView.getImageSize());
        add(statusBar, BorderLayout.SOUTH);
        statusBar.refresh();
        super.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentShown(ComponentEvent e) {
                observer = (ImageView.ImageViewObserver) (z) -> {
                    statusBar.zoom = z;
                    statusBar.refresh();
                };
                imageView.addObserver(observer);
                statusBar.zoom = imageView.getZoom();
                statusBar.refresh();
            }
            @Override
            public void componentHidden(ComponentEvent e) {
                imageView.removeObserver(observer);
            }
        });
    }

    /**
     * Perform specified (shortcut) action. Handles zooming, scrolling,
     * go-to-previous/next-image, go up (to folder view).
     * @param a Action to perform.
     */
    public void onAction(Jimgview.Action a) {
        int signum = 0;
        JScrollBar scrollbar = null;
        switch (a) {
        case ZOOM_FIT: imageView.setFitInWindow(true); break;
        case ZOOM_1TO1: setZoom(1.0);     break;
        case ZOOM_IN:   modifyZoom(1.20); break;
        case ZOOM_OUT:  modifyZoom(0.80); break;
        case GO_PREV:   Jimgview.prev();  break;
        case GO_NEXT:   Jimgview.next();  break;
        case GO_UP:
            Jimgview.loadPath(imageView.getPath().getParentFile());
            break;
        case SCROLL_UP:
            signum = -1;
        case SCROLL_DOWN:
            if (signum == 0) signum = 1;
            scrollbar = imageView.getVerticalScrollBar();
        case SCROLL_LEFT:
            if (signum == 0) signum = -1;
        case SCROLL_RIGHT:
            if (signum == 0) signum = 1;
            if (scrollbar == null) scrollbar = imageView.getHorizontalScrollBar();
            scrollbar.setValue(scrollbar.getValue()+16*signum);
            break;
        }
    }

    /**
     * Returns control components to be displayed wherever controlling
     * component wants them.
     * @return Array of control components (right now only buttons).
     */
    public JComponent[] getToolBarItems() {
        return new JComponent[] {
            ComponentBuilder.jButton("fit_to_window", "action_fit"  , e -> imageView.setFitInWindow(true)),
            ComponentBuilder.jButton("zoom_100pct"  , "action_1to1" , e -> setZoom(1.0)),
            ComponentBuilder.jButton("zoom_plus"    , "action_plus" , e -> modifyZoom(1.20)),
            ComponentBuilder.jButton("zoom_minus"   , "action_minus", e -> modifyZoom(0.80)),
            ComponentBuilder.jButton("prev"         , "action_left" , e -> Jimgview.prev()),
            ComponentBuilder.jButton("next"         , "action_right", e -> Jimgview.next()),
            ComponentBuilder.jButton("up"           , "action_up"   , e -> {
                Jimgview.loadPath(imageView.getPath().getParentFile());
            })
        };
    }

    /**
     * Set zoom value. Command is forwarded to {@link ImageView}.
     * @param zoom The requested zoom value. 1.0 = 100%
     */
    private void setZoom(double zoom) {
        imageView.setZoom(zoom);
    }

    /**
     * Modifies zoom value. Forwarded to {@link ImageView}. Multiplies current
     * zoom value by provided value.
     * @param zoom New zoom value in % of current zoom value.
     *     Example: 1.20 will magnify image by 20%.
     */
    private void modifyZoom(double zoom) {
        imageView.setZoom(imageView.getZoom()*zoom);
    }

    /**
     * Returns File object of the image which is currently being displayed
     * @return the image File (not guaranteed to be valid path)
     */
    public File getPath() { return imageView.getPath(); }

    /**
     * Simple label extension displaying information about current image.
     * Displays size, zoom, and "index of current image/number of images".
     * Constructor uses registers ImageViewObserver to update the status bar
     * with up-to-date values.
     */
    private class StatusBar extends JLabel {
        /** format for shing zoom % */
        private DecimalFormat ZOOM_FORMAT = new DecimalFormat("#.##");
        
        /** current zoom */
        public Double zoom;
        /** current size */
        public Dimension size;

        /**
         * Create status bar, with initial zoom and size
         * @param zoom The current zoom value.
         * @param size The current size.
         */
        public StatusBar(Double zoom, Dimension size) {
            this.zoom = zoom;
            this.size = size;
            super.setPreferredSize(new Dimension(100, 15));
        }

        /**
         * Refresh the displayed text.
         */
        public void refresh() {
            String text = "  "
                + Integer.toString(Jimgview.folder.getSelectedImageIdx())
                + "/" + Integer.toString(Jimgview.folder.getImagesTotal())
                + "  ";
            if (size != null) {
                text += (Integer.toString(size.width) + "x" +
                        Integer.toString(size.height) + "  ");
            }
            if (zoom != null) {
                text += (ZOOM_FORMAT.format(zoom*100) + "%  ");
            }
            setText(text);
            revalidate();
        }
    }
}
